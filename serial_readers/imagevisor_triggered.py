# -*- coding: utf-8 -*-
'''
This code comunicates with Arduino to requesting pictures
and taking them with blocking commands.
Reads the data, searches image start and end headers
and detect if the images are a door or not.
It is poosible that arduino needs to be restarted
to synchronize with this code.

The correspondant Arduino code is at:
https://bitbucket.org/snippets/jjv_team/KrbA/lscam-triggered-photo-loop
in ls_triggered_photo_loop.ino

Created on 3/5/2015

@author: Javi
'''


from serial import Serial
import binascii
import cv2
import numpy as np

def door_detector(image, show=False):
    imageHSV = cv2.cvtColor(image, cv2.cv.CV_RGB2HSV)
    redOnly = cv2.inRange(imageHSV, cv2.cv.Scalar(85, 5, 5), cv2.cv.Scalar(115, 255, 250));
    is_door = cv2.mean(redOnly)[0]>255/3     # door comparison threshold
    if(show):
        detector = np.zeros([100, 100, 3], np.uint8, 3)
        detector[:,:,2]=255*is_door
        cv2.imshow("H", imageHSV[:,:,0])
        cv2.imshow("S", imageHSV[:,:,1])
        cv2.imshow("V", imageHSV[:,:,2])
        cv2.imshow("detector", detector)
        cv2.imshow("door detection", redOnly)
    return is_door


def content_to_image(content):
    arr = np.asarray(bytearray(content), dtype=np.uint8)
    im = cv2.imdecode(arr, cv2.CV_LOAD_IMAGE_COLOR)
    return im

# open serial port
ser = Serial('COM6',115200,timeout=None) # leemos del puerto que nos indique Arduino
# timeout = timeout de lectura, no de conexion, si tarda mas de ese tiempo, devuelve -1
# con None lo hacemos bloqueante


# leemos el primer byte para inicializar las variables
hex1 = ser.read(1)
hex2 = hex1
print 'Arduino flag received!'
ser.write('!') # enviamos un byte para tomar la primera imagen

while True:
    print hex1,
    # convertimos los bytes leidos en hexademical como texto ascii
    minor = binascii.b2a_hex(hex1)
    major = binascii.b2a_hex(hex2)
    # para poder comparar con sus valores con los valores
    # hexadecimales conocidos como cabecera de inicio de imagen
    if((major=="ff") and (minor=="d8")):
        print "Image detected"
        arr = [hex2, hex1]
        while(not((major=="ff") and (minor=="d9"))):
            hex2 = hex1
            hex1 = ser.read()
            arr = arr + [hex1]
            minor = binascii.b2a_hex(hex1)
            major = binascii.b2a_hex(hex2)
        print "Image finished"
        image = content_to_image(arr)
        cv2.imshow("Image",image)
        door_detector(image, True)
        cv2.waitKey(33)
        ser.write('!') # seguimos tomando más imágenes
    # Actualizamos los valores avanzando solo 1 byte ya que no sabemos
    # si hemos comenzado a leer en una posici�n par o impar
    hex2 = hex1
    hex1 = ser.read()
    pass
