# -*- coding: utf-8 -*-
'''
Reads from serial port in binary, searches image
start and end headers and write the images in an output_x.jpg file
in the image folder (CREATE THIS FOLDER PREVIOUSLY)
Created on 13/4/2015

@author: Javi
'''

from serial import Serial
import binascii

# open serial port
ser = Serial('COM6',115200,timeout=2) # leemos del puerto que nos indique Arduino
im_num = 0

# leemos el primer byte para inicializar las variables
hex1 = ser.read()
hex2 = hex1

while True:
    # convertimos los bytes leidos en hexademical como texto ascii
    minor = binascii.b2a_hex(hex1)
    major = binascii.b2a_hex(hex2)
    # para poder comparar con sus valores con los valores
    # hexadecimales conocidos como cabecera de inicio de imagen
    if((major=="ff") and (minor=="d8")):
        print "Imagen detectada"
        # Abrimos el fichero donde vamos a guardar la imagen
        nf = open("images/output_"+str(im_num)+".jpg", "wb")
        nf.write(hex2)
        nf.write(hex1)
        im_num += 1
        # y vamos escribiendo todos los bytes hasta la cabecera de fin de imagen
        while(not((major=="ff") and (minor=="d9"))):
            hex2 = hex1
            hex1 = ser.read()
            nf.write(hex1)
            minor = binascii.b2a_hex(hex1)
            major = binascii.b2a_hex(hex2)
            print hex1,
        print ""
        nf.write(hex2)
        nf.write(hex1)
        nf.close()
        print "Imagen acabada"
    # Actualizamos los valores avanzando solo 1 byte ya que no sabemos
    # si hemos comenzado a leer en una posici�n par o impar
    hex2 = hex1
    hex1 = ser.read()
    pass