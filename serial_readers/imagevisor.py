# -*- coding: utf-8 -*-
'''
Reads from serial port in binary, searches image
start and end headers and shows the images with opencv
Created on 13/4/2015

@author: Javi
'''

from serial import Serial
import binascii
import cv2
import numpy as np

def detector(image):
    pass


def content_to_image(content):
    arr = np.asarray(bytearray(content), dtype=np.uint8)
    im = cv2.imdecode(arr, cv2.CV_LOAD_IMAGE_COLOR)
    return im



# open serial port
ser = Serial('COM6',115200,timeout=None) # leemos del puerto que nos indique Arduino
# timeout = timeout de lectura, no de conexion, si tarda mas de ese tiempo, devuelve -1
# con None lo hacemos bloqueante


# leemos el primer byte para inicializar las variables
hex1 = ser.read()
hex2 = hex1
while True:
    print hex1,
    # convertimos los bytes leidos en hexademical como texto ascii
    minor = binascii.b2a_hex(hex1)
    major = binascii.b2a_hex(hex2)
    # para poder comparar con sus valores con los valores
    # hexadecimales conocidos como cabecera de inicio de imagen
    if((major=="ff") and (minor=="d8")):
        print "Imagen detectada"
        arr = [hex2, hex1]
        while(not((major=="ff") and (minor=="d9"))):
            hex2 = hex1
            hex1 = ser.read()
            arr = arr + [hex1]
            minor = binascii.b2a_hex(hex1)
            major = binascii.b2a_hex(hex2)
        print arr
        image = content_to_image(arr)
        cv2.imshow("Imagen",image)
        cv2.waitKey(33)
        detector(image)
        print ""
        print "Imagen acabada"
    # Actualizamos los valores avanzando solo 1 byte ya que no sabemos
    # si hemos comenzado a leer en una posici�n par o impar
    hex2 = hex1
    hex1 = ser.read()
    pass
