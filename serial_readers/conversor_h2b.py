'''
Convert a file hexa.txt with hexadecimal string
values to a binary jpeg file

Created on 13/4/2015

@author: Javi
'''
import binascii


f = open("hexa.txt","r")
nf = open("output.jpg", "wb")
count = 0
while 1:
    c = f.readline()
    d = c.strip()
    print count, c, d
    count+=1
    if not c:
        break
    nf.write(binascii.a2b_hex(d))
f.close()
nf.close()