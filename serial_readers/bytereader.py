'''
Created on 13/4/2015

@author: Javi
'''

import binascii

image1 = open("images/output_78.jpg", "r")
image2 = open("images/output_79.jpg", "r")

byte = image1.read(1)
while( byte > 0):
    ascii = binascii.b2a_hex(byte)
    print byte,
    print [ascii],
    if(byte!=''):
        imsize = int("0x" + ascii, 0)
        print imsize

    byte = image1.read(1)

image1.close()
image2.close()