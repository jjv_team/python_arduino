'''
Reads from serial port and prints its content
in binary, hexadecimal and decimal
Created on 13/4/2015

@author: Javi
'''

from serial import Serial
import binascii

# open serial port
ser = Serial('COM6',115200)
 
 
while True:
    cadena1 = ser.read()
    cadena2 = ser.read()
    major = binascii.b2a_hex(cadena1)
    minor = binascii.b2a_hex(cadena2)
    imsize = int("0x" + major + minor, 0)
    print cadena1,cadena2, "|",
    print major,minor, "|",
    print imsize
    pass
