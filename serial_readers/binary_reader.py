'''
Convert a file binary.txt with binary string
values to a binary jpeg file

Created on 13/4/2015

@author: Javi
'''
import binascii


f = open("binary.txt","r")
nf = open("output.jpg", "wb")
count = 0
while 1:
    c = f.readline()
    d = c.strip()
    print count, c, d
    count+=1
    if not c:
        break
    nf.write(binascii.b2a_hex(d))
f.close()
nf.close()