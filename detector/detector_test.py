'''
Pruebas para generar un filtro de color
para detectar puertas rojas en el espacio RGB
Created on 3/5/2015

@author: Javi
'''

import cv2
import numpy as np


image = cv2.imread('puerta.jpg')
print image
cv2.imshow("Imagen", image)
redOnly = cv2.inRange(image, cv2.cv.Scalar(30, 30, 50), cv2.cv.Scalar(120, 120, 255));

cv2.imshow("canal rojo", redOnly)

detector = np.zeros([100, 100, 3], np.uint8, 3)
detector[:,:,2]=255

print detector
cv2.imshow("detector", detector)

cv2.waitKey()

