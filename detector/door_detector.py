'''
detects if an image is a door or not
Created on 3/5/2015

@author: Javi
'''

import cv2
import numpy as np

def door_detector(image, show=False):
    imageHSV = cv2.cvtColor(image, cv2.cv.CV_RGB2HSV)
    redOnly = cv2.inRange(imageHSV, cv2.cv.Scalar(100, 100, 50), cv2.cv.Scalar(110, 255, 150));
    is_door = cv2.mean(redOnly)[0]>255/2
    if(show):
        detector = np.zeros([100, 100, 3], np.uint8, 3)
        detector[:,:,2]=255*is_door     # door comparison threshold
        cv2.imshow("detector", detector)
    return is_door


if __name__ == '__main__':
    puerta = cv2.imread('puerta.jpg')
    cv2.imshow("imagen", puerta)
    print "es puerta?", door_detector(puerta, True)
    cv2.waitKey()
    nopuerta = cv2.imread('no_puerta.jpg')
    cv2.imshow("imagen", nopuerta)
    print "es puerta?", door_detector(nopuerta, True)
    cv2.waitKey()
