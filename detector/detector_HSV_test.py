'''
Pruebas para generar un filtro de color
para detectar puertas rojas en el espacio HSV
Created on 3/5/2015

@author: Javi
'''

import cv2
import numpy as np

def hist_lines(im):
    h = np.zeros((300,256,3))
    if len(im.shape)!=2:
        print "hist_lines applicable only for grayscale images"
        #print "so converting image to grayscale for representation"
        im = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
    hist_item = cv2.calcHist([im],[0],None,[256],[0,256])
    cv2.normalize(hist_item,hist_item,0,255,cv2.NORM_MINMAX)
    hist=np.int32(np.around(hist_item))
    for x,y in enumerate(hist):
        cv2.line(h,(x,0),(x,y),(255,255,255))
    y = np.flipud(h)
    return y

image = cv2.imread('puerta.jpg')
cv2.imshow("Imagen", image)
imageHSV = cv2.cvtColor(image, cv2.cv.CV_RGB2HSV)
histHSV = hist_lines(imageHSV[:,:,0])
print np.min(imageHSV[:,:,0]), np.max(imageHSV[:,:,0]), cv2.mean(imageHSV[:,:,0])
cv2.imshow("hist HSV", histHSV)
redOnly = cv2.inRange(imageHSV, cv2.cv.Scalar(100, 100, 50), cv2.cv.Scalar(110, 255, 150));

cv2.imshow("canal rojo", redOnly)

detector = np.zeros([100, 100, 3], np.uint8, 3)
detector[:,:,2]=255

cv2.imshow("detector", detector)

cv2.waitKey()

